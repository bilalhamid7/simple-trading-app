import { BigNumber } from 'bignumber.js';
import { isNil, isEmpty } from 'lodash';
import { IStockDetails, IStockType, ITrade } from '../models/trade';

export const getDividendYield = (stockDetail:IStockDetails):number => {
  if (stockDetail.type === IStockType.Common) {
    return getCommonDividendYield(stockDetail.lastDividend, stockDetail.currentPrice);
  }
  else if (stockDetail.type === IStockType.Preferred) {
    return getPreferredDividendYield(stockDetail.fixedDividend, stockDetail.parValue, stockDetail.currentPrice);
  }
  return null;
};

export const getCommonDividendYield = (lastDividend:number, price:number):number => {
  if (isNil(lastDividend) || isNil(price)) {
    return null;
  }
  const dividendYield = new BigNumber(lastDividend)
    .div(price)
    .times(100)
    .toFixed(2);
  return parseFloat(dividendYield);
};

export const getPreferredDividendYield = (fixedDividend:number, parValue:number, price:number):number => {
  if (isNil(fixedDividend) || isNil(price) || isNil(price)) {
    return null;
  }
  const dividendYield = new BigNumber(fixedDividend)
    .times(parValue)
    .div(price)
    .toFixed(2);
  return parseFloat(dividendYield);
};

export const getPeRatio = (price:number, dividend:number):number => {
  if (isNil(price) || isNil(dividend)) {
    return null;
  }
  const peRatio = new BigNumber(price)
    .div(dividend)
    .toFixed(2);
  return parseFloat(peRatio);
};

export const getGeometricMean = (trades:ITrade[]):number => {
  if (isEmpty(trades)) {
    return null;
  }

  const sumProduct = trades.reduce((prev, cur) => prev * cur.tradePrice, 1);
  const nthPower = parseFloat(new BigNumber(1).div(new BigNumber(trades.length)).toFixed());

  return parseFloat(Math.pow(sumProduct, nthPower).toFixed(2));
};

export const getVolumeWeightedStockPrice = (trades:ITrade[]):number => {
  if (isEmpty(trades)) {
    return null;
  }

  const weightedValue = trades.reduce((prev, cur) => prev + cur.tradePrice * cur.quantity, 0);
  const quantitySum = trades.reduce((prev, cur) => prev + cur.quantity, 0);

  const volumeWeightedStockPrice = new BigNumber(weightedValue)
    .div(quantitySum)
    .toFixed(2);

  return parseFloat(volumeWeightedStockPrice);
};


