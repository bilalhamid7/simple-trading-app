import { stockDetailsFactory } from '../__test_utils__/stockDetailsFactory';
import { getCommonDividendYield, getDividendYield, getPreferredDividendYield } from './stockMetric';
import { IStockType } from '../models';

describe('stockMetric', () => {

  describe('getDividendYield', () => {
    it('calls getCommonDividendYield if stock type is common', () => {
      // Arrange
      const stockDetail = stockDetailsFactory.build();

      // Act
      const result = getDividendYield(stockDetail);

      // Assert
      expect(result).toBe(getCommonDividendYield(stockDetail.lastDividend, stockDetail.currentPrice));
    });

    it('calls getPreferredDividendYield if stock type is preferred', () => {
      // Arrange
      const stockDetail = stockDetailsFactory.with({type: IStockType.Preferred}).build();

      // Act
      const result = getDividendYield(stockDetail);

      // Assert
      expect(result).toBe(
        getPreferredDividendYield(stockDetail.fixedDividend, stockDetail.parValue, stockDetail.currentPrice)
      );
    });

    it('return null if price is null', () => {
      // Arrange
      const stockDetail = stockDetailsFactory.with({currentPrice: null}).build();

      // Act
      const result = getDividendYield(stockDetail);

      // Assert
      expect(result).toBe(null);
    });

  });

});
