import createReducer from 'redux-action-reducer';
import { StockMarketActions } from '../actions';
import { IStockDetails, ITrade, ITradeForm } from '../models/trade';
import { AnyAction, combineReducers } from 'redux';
import { IStockMarketApp } from '../models';
import { INITIAL_STATE } from './initialState';

/**
 * Using createReducer reduces the amount of boiler plate code written.
 * Multiple different actions can be handled in one reducer.
 *
 * https://www.npmjs.com/package/redux-action-reducer
 */

const trades = createReducer(
  [StockMarketActions.FETCH_TRADES_SUCCESS, (state:ITrade[], payload:{ trades:ITrade[] }) => payload.trades]
)([]);

const stockDetails = createReducer(
  [
    StockMarketActions.FETCH_STOCK_DETAILS_SUCCESS,
    (state:ITrade[], payload:{ stockDetails:IStockDetails[] }) => payload.stockDetails
  ]
)([]);

const tradeForm = createReducer(
  [
    StockMarketActions.UPDATE_FORM_FIELD,
    (state:ITradeForm, payload:{ [id:string]:any }) => {
      return {
        ...state,
        ...payload
      }
    }
  ]
)(INITIAL_STATE.tradeForm);

export const stockMarketAppReducer = combineReducers<IStockMarketApp, AnyAction>({
  trades,
  stockDetails,
  tradeForm
});
