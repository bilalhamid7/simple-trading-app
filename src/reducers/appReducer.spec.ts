import { stockMarketAppReducer as reducer } from './appReducer';
import { defaultTradeForm, INITIAL_STATE } from './initialState';
import { IStockMarketApp } from '../models';
import { StockMarketActionTypes } from '../actions/actionTypes';
import { StockMarketActions } from '../actions';
import { tradeFactory } from '../__test_utils__/tradeFactory';
import { stockDetailsFactory } from '../__test_utils__/stockDetailsFactory';

describe('app reducer', () => {
  it('returns default state when no state is given', () => {
    // Act
    const newState = reducer(undefined, {
      type: 'no-match'
    });

    // Assert
    expect(newState).toEqual(INITIAL_STATE);
  });

  it('returns the same state when action is not matched', () => {
    // Arrange
    const state:IStockMarketApp = {
      ...INITIAL_STATE
    };

    // Act
    const newState = reducer(state, {
      type: 'no-match'
    });

    // Assert
    expect(newState).toBe(state);
  });

  describe('trades reducer', () => {
    it('is initialised with an empty array', () => {
      // Act
      const state:IStockMarketApp = {
        ...INITIAL_STATE
      };

      // Assert
      expect(state.trades).toEqual([]);
    });

    it('updates trade state when getTrades returns success', () => {
      const state:IStockMarketApp = {
        ...INITIAL_STATE
      };

      const newTradeList = tradeFactory.buildMany(1);

      const action:StockMarketActionTypes = {
        type: StockMarketActions.FETCH_TRADES_SUCCESS,
        payload: {
          trades: newTradeList
        }
      };

      // Act
      const newState = reducer(state, action);

      // Assert
      expect(newState.trades).toEqual(newTradeList);
    });
  });

  it('returns the same state when action is not matched', () => {
    // Arrange
    const state:IStockMarketApp = {
      ...INITIAL_STATE
    };

    // Act
    const newState = reducer(state, {
      type: 'no-match'
    });

    // Assert
    expect(newState).toBe(state);
  });

  describe('stock details reducer', () => {
    it('is initialised with an empty array', () => {
      // Act
      const state:IStockMarketApp = {
        ...INITIAL_STATE
      };

      // Assert
      expect(state.stockDetails).toEqual([]);
    });

    it('updates trade state when getTrades returns success', () => {
      const state:IStockMarketApp = {
        ...INITIAL_STATE
      };

      const newStockDetails = stockDetailsFactory.buildMany(1);

      const action:StockMarketActionTypes = {
        type: StockMarketActions.FETCH_STOCK_DETAILS_SUCCESS,
        payload: {
          stockDetails: newStockDetails
        }
      };

      // Act
      const newState = reducer(state, action);

      // Assert
      expect(newState.stockDetails).toEqual(newStockDetails);
    });
  });

  describe('tradeForm', () => {
    it('is initialised with empty values by default', () => {
      // Act
      const state:IStockMarketApp = {
        ...INITIAL_STATE
      };

      // Assert
      expect(state.tradeForm).toBe(defaultTradeForm);
    });

    it('updates fields value when they change', () => {
      const state:IStockMarketApp = {
        ...INITIAL_STATE
      };

      const action:StockMarketActionTypes = {
        type: StockMarketActions.UPDATE_FORM_FIELD,
        payload: {
          ticker: 'new value'
        }
      };

      // Act
      const newState = reducer(state, action);

      // Assert
      expect(newState.tradeForm).toEqual({
        ...INITIAL_STATE.tradeForm,
        ticker: action.payload.ticker
      });
    });
  });

});
