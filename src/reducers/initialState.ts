import { IStockMarketApp } from '../models';
import { ITradeForm } from '../models/trade';

export const defaultTradeForm:ITradeForm = {
  ticker: '',
  tradePrice: null,
  quantity: null,
  tradeType: null,
  isDirty: false
};

export const INITIAL_STATE:IStockMarketApp = {
  trades: [],
  stockDetails: [],
  tradeForm: defaultTradeForm
};
