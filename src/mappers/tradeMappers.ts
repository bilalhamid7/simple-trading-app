import moment from 'moment';

import { IStockDetails, ITrade, ITradeForm } from '../models/trade';

export const mapTrades = (trades:ITrade[]) => trades.map(mapTrade);

export const mapTrade = (trade:ITrade) => ({
  ...trade,
  tradeTime: moment(trade.tradeTime).toDate(),
  value: trade.quantity * trade.tradePrice
});

export const mapStockDetails = (stockDetails:IStockDetails[]) => stockDetails.map(mapStockDetail);

export const mapStockDetail = (stockDetail:IStockDetails) => ({
  ...stockDetail
});

export const getSubmitTradeRequest = (tradeForm:ITradeForm):ITrade => ({
  ...tradeForm,
  tradeTime: new Date(),
  value: tradeForm.tradePrice * tradeForm.quantity
});
