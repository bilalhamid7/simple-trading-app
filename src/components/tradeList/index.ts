import { connect } from 'react-redux';

import { IProps, TradeListView } from './views/tradeListView';
import selector from './selector';

export const TradeList = connect<IProps>(
  selector
)(TradeListView);
