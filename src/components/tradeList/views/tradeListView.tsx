import * as React from 'react';

import { ITrade } from '../../../models/trade';
import { formatDate } from '../../../utils/formatters';

export interface IProps {
  trades:ITrade[];
}

export const TradeListView = (props:IProps) => (
  <>
    <h2>Trade List</h2>
    <table className="table">
      <thead>
      <tr>
        <th>Stock Symbol</th>
        <th>Type</th>
        <th>Trade Price (£)</th>
        <th>Trade Date</th>
        <th>Quantity</th>
        <th>Value (£)</th>
      </tr>
      </thead>
      <tbody>
      {props.trades.map((trade:ITrade, index:number) => (
        <tr key={index}>
          <td>{trade.ticker}</td>
          <td>{trade.tradeType}</td>
          <td>{trade.tradePrice}</td>
          <td>{formatDate(trade.tradeTime)}</td>
          <td>{trade.quantity}</td>
          <td>{trade.value}</td>
        </tr>
      ))}
      </tbody>
    </table>
  </>
);
