import * as React from 'react';
import { TradeList } from '../tradeList';
import 'bulma/css/bulma.css';
import { Link } from 'react-router-dom';
import { constants } from '../../constants';
import { StockMetricsList } from '../stockMetrics';

export const HomePage = () => (
  <div className="App">
    <div className="columns">
      <div className="column is-offset-2 is-8">
        <TradeList/>
      </div>
      <div className="column">
        <Link to={constants.routes.newTrade}>
          <button className="button is-primary is-pulled-right"> New Trade</button>
        </Link>
      </div>
    </div>
    <div className="columns">
      <div className="column is-offset-2 is-8">
        <StockMetricsList/>
      </div>
    </div>
  </div>
);
