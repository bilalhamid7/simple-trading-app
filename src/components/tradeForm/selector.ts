import { createSelector } from 'reselect';
import { getTradeForm } from '../../selectors/tradeSelectors';
import { getTradeFormErrors } from './formRules';

export default createSelector(
  getTradeForm,
  getTradeFormErrors,
  (tradeForm, formErrors) => ({
    tradeForm,
    formErrors: tradeForm.isDirty ? formErrors : {}
  })
);
