import { connect } from 'react-redux';

import { IDispatchProps, IProps, TradeFormView } from './views/tradeFormView';
import selector from './selector';
import actions from './actions';

export const TradeForm = connect<IProps, IDispatchProps>(
  selector,
  actions
)(TradeFormView);
