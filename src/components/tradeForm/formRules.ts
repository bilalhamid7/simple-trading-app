import { createSelector } from 'reselect';
import { notEmpty, validate, isNumeric } from '../../utils/validation';
import { getTradeForm } from '../../selectors/tradeSelectors';

const rules = {
  ticker: [
    {test: notEmpty, message: 'cannot be empty'}
  ],
  tradePrice: [
    {test: notEmpty, message: 'cannot be empty'},
    {test: isNumeric, message: 'not a valid number'},
  ],
  quantity: [
    {test: notEmpty, message: 'cannot be empty'},
    {test: isNumeric, message: 'not a valid number'},
  ]
};

export const getTradeFormErrors = createSelector(
  getTradeForm,
  (tradeForm) => validate(rules)(tradeForm)
);
