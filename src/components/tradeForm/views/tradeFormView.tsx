import * as React from 'react';
import { isEmpty } from 'lodash';

import { ITradeForm } from '../../../models/trade';
import { TextInput } from '../../common/forms/TextInput';
import { Link } from 'react-router-dom';
import { constants } from '../../../constants';

export interface IProps {
    tradeForm:ITradeForm;
    formErrors:{ [field:string]:string };
}

export interface IDispatchProps {
    submitTrade:() => any;
    updateFormField:(payload:{ [id:string]:any }) => any;
}

export type TradeFormProps = IProps & IDispatchProps;

export const TradeFormView = (props:TradeFormProps) => (
    <div className="container">
        <div className="columns">
            <div className="column is-6 is-offset-2">
                <div className="box">
                    <form>
                        <div>
                            <TextInput
                                componentId={'ticker'}
                                label={'Stock Symbol'}
                                value={props.tradeForm.ticker.toUpperCase()}
                                onFieldChange={props.updateFormField}
                                error={props.formErrors.ticker}
                            />
                        </div>
                        <div>
                            <TextInput
                                componentId={'tradePrice'}
                                label={'Price'}
                                value={props.tradeForm.tradePrice}
                                onFieldChange={props.updateFormField}
                                error={props.formErrors.tradePrice}
                            />
                        </div>
                        <div>
                            <TextInput
                                componentId={'quantity'}
                                label={'Quantity'}
                                value={props.tradeForm.quantity}
                                onFieldChange={props.updateFormField}
                                error={props.formErrors.quantity}
                            />
                        </div>

                        <button
                            className="button is-primary"
                            type="button"
                            onClick={props.submitTrade}
                            disabled={!isEmpty(props.formErrors)}
                        >
                            Submit
                        </button>
                    </form>
                </div>
            </div>
            <div className="column">
                <Link to={constants.routes.root}>
                    <button className="button is-primary is-pulled-right">
                        Back
                    </button>
                </Link>
            </div>
        </div>
    </div>
);
