import { submitTrade, updateFormField } from '../../actions';

export default {
  submitTrade,
  updateFormField
};
