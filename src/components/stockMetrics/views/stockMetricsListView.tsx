import * as React from 'react';

import { IStockMetrics } from '../../../models/trade';

export interface IProps {
  stockMetrics:IStockMetrics[];
}

export const StockMetricsListView = (props:IProps) => (
  <>
    <h2>Stock Metrics</h2>
    <table className="table">
      <thead>
      <tr>
        <th>Stock Symbol</th>
        <th>Current Price</th>
        <th>Yield</th>
        <th>P/E Ratio</th>
        <th>Geometric Mean</th>
        <th>Volume Weighted Stock Price</th>
      </tr>
      </thead>
      <tbody>
      {props.stockMetrics.map((stock:IStockMetrics, index:number) => (
        <tr key={index}>
          <td>{stock.ticker}</td>
          <td>{stock.currentPrice}</td>
          <td>{stock.yield}</td>
          <td>{stock.peRatio}</td>
          <td>{stock.geometricMean}</td>
          <td>{stock.volumeWeightedStockPrice}</td>
        </tr>
      ))}
      </tbody>
    </table>
  </>
);
