import { shallow } from 'enzyme';
// @ts-ignore
import * as React from 'react';
import { create as createSnapshot } from 'react-test-renderer';

// @ts-ignore
import { IProps, StockMetricsListView } from './stockMetricsListView';

describe('<StockMetricsList />', () => {
  const props:IProps = {
    stockMetrics: []
  };

  it('renders the component as expected', () => {
    // Arrange
    const cmp = <StockMetricsListView {...props} />;

    // Act
    const el = shallow(cmp);
    const snapshot = createSnapshot(cmp).toJSON();

    // Assert
    expect(el.find('h2').text()).toEqual('Stock Metrics');

    expect(snapshot).toMatchSnapshot();
  });

});
