import { connect } from 'react-redux';

import { IProps, StockMetricsListView } from './views/stockMetricsListView';
import selector from './selector';

export const StockMetricsList = connect<IProps>(
  selector
)(StockMetricsListView);
