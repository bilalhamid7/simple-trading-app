import { createSelector } from 'reselect';
import { getStockMetrics } from '../../selectors/tradeSelectors';

export default createSelector(
  getStockMetrics,
  (stockMetrics) => ({
    stockMetrics
  })
);
