import { createSelector } from 'reselect';

import { IStockMarketApp } from '../models';
import { IStockDetails, IStockMetrics, ITrade } from '../models/trade';
import { getDividendYield, getPeRatio, getVolumeWeightedStockPrice, getGeometricMean } from '../utils/stockMetric';

export const getTrades = createSelector(
  (state:IStockMarketApp) => state,
  (state) => state.trades
);

/**
 * Returns trades in reverse chronological order. Uses re-select for memoization.
 */
export const getTradesInReverseChronologicalOrder = createSelector(
  getTrades,
  (trades) => trades.sort((a:ITrade, b:ITrade) => b.tradeTime.getTime() - a.tradeTime.getTime())
);

export const getTradeForm = createSelector(
  (state:IStockMarketApp) => state,
  (state) => state.tradeForm
);

export const getStockDetails = createSelector(
  (state:IStockMarketApp) => state,
  (state) => state.stockDetails
);

export const getStockMetrics = createSelector(
  getTrades,
  getStockDetails,
  (trades, stockDetails) => stockDetails.map((stock:IStockDetails) => {
    const dividendYield = getDividendYield(stock);
    const filteredTrades = trades.filter((o:ITrade) => o.ticker === stock.ticker);

    const stockMetric:IStockMetrics = {
      ...stock,
      yield: dividendYield,
      peRatio: getPeRatio(stock.currentPrice, stock.lastDividend),
      geometricMean: getGeometricMean(filteredTrades),
      volumeWeightedStockPrice: getVolumeWeightedStockPrice(filteredTrades)
    };
    return stockMetric;
  })
);
