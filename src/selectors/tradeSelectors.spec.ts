import * as selectors from './tradeSelectors';
import { IStockMarketApp, IStockMetrics } from '../models';
import { tradeFactory } from '../__test_utils__/tradeFactory';
import { stockDetailsFactory } from '../__test_utils__/stockDetailsFactory';

describe('tradeSelectors', () => {
  let state:IStockMarketApp;

  describe('getTrades', () => {
    it('returns empty list when there are no trades', () => {
      // Arrange
      state = {
        trades: []
      } as any;

      // Act
      const result = selectors.getTrades(state);

      // Assert
      expect(result).toEqual([]);
    });

    it('returns trades when there are trades', () => {
      // Arrange
      const trades = tradeFactory.buildMany(1);
      state = {
        trades
      } as any;

      // Act
      const result = selectors.getTrades(state);

      // Assert
      expect(result).toEqual(trades);
    });
  });

  describe('getTradesInReverseChronologicalOrder', () => {
    it('returns trades in reverse chronological order', () => {
      // Arrange
      const unsortedTrades = [
        tradeFactory.with({tradeTime: new Date('2018-01-31')}).build(),
        tradeFactory.with({tradeTime: new Date('2018-03-05')}).build(),
        tradeFactory.with({tradeTime: new Date('2018-01-02')}).build(),
        tradeFactory.with({tradeTime: new Date('2017-12-20')}).build()
      ];

      const sortedTrades = [
        tradeFactory.with({tradeTime: new Date('2018-03-05')}).build(),
        tradeFactory.with({tradeTime: new Date('2018-01-31')}).build(),
        tradeFactory.with({tradeTime: new Date('2018-01-02')}).build(),
        tradeFactory.with({tradeTime: new Date('2017-12-20')}).build()
      ];

      state = {
        trades: unsortedTrades
      } as any;

      // Act
      const result = selectors.getTradesInReverseChronologicalOrder(state);

      // Assert
      expect(result).toEqual(sortedTrades);
    });
  });

  describe('getStockDetails', () => {
    it('returns empty list when there are no stock details', () => {
      // Arrange
      state = {
        stockDetails: []
      } as any;

      // Act
      const result = selectors.getStockDetails(state);

      // Assert
      expect(result).toEqual([]);
    });

    it('returns trades when there are trades', () => {
      // Arrange
      const stockDetails = stockDetailsFactory.buildMany(1);
      state = {
        stockDetails
      } as any;

      // Act
      const result = selectors.getStockDetails(state);

      // Assert
      expect(result).toEqual(stockDetails);
    });
  });

  describe('getStockMetrics', () => {
    it('returns empty list when there are no stock details', () => {
      // Arrange
      state = {
        trades: [],
        stockDetails: []
      } as any;

      // Act
      const result = selectors.getStockMetrics(state);

      // Assert
      expect(result).toEqual([]);
    });

    it('returns stock metrics when there are stock details', () => {
      // Arrange
      const trades = tradeFactory.buildMany(1);
      const stockDetails = stockDetailsFactory.buildMany(1);
      state = {
        trades,
        stockDetails
      } as any;

      // Act
      const result = selectors.getStockMetrics(state);

      // Assert
      result.forEach((stockMetric:IStockMetrics) => {
        expect(stockMetric).toHaveProperty('ticker');
        expect(stockMetric).toHaveProperty('yield');
        expect(stockMetric).toHaveProperty('peRatio');
        expect(stockMetric).toHaveProperty('geometricMean');
        expect(stockMetric).toHaveProperty('volumeWeightedStockPrice');

        expect(stockMetric.ticker).toBe('ticker');
      })
    });
  });

});
