import { Epic, createEpicMiddleware } from 'redux-observable';
import { AnyAction } from 'redux';
import { VirtualTimeScheduler } from 'rxjs';
import configureMockStore from 'redux-mock-store';

// Force rxjs to load into window
export * from 'rxjs';

export function testEpic(epic:Epic) {
  return new EpicTester(epic);
}

function createEpicMockStore<TState>(state?:TState) {
  const epicMiddleware = createEpicMiddleware();
  const middleware = [epicMiddleware];
  const mockStore = configureMockStore<TState>(middleware)(state);
  return {mockStore, epicMiddleware};
}

type ActionHandler = (action:AnyAction) => any;

class EpicTester<TState> {
  constructor(private epic:Epic) {
    this.handlers = [];
  }

  private state:TState;
  private scheduler:VirtualTimeScheduler;
  private handlers:ActionHandler[];

  withState(state:TState) {
    this.state = state;
    return this;
  }

  withScheduler(scheduler:VirtualTimeScheduler) {
    this.scheduler = scheduler;
    return this;
  }

  onAction(handler:ActionHandler) {
    this.handlers.push(handler);
    return this;
  }

  whenReceivesAction(action:AnyAction) {
    return {
      emits: async (...expected:AnyAction[]) => {
        return new Promise(async (resolve) => {
          const {mockStore, epicMiddleware} = createEpicMockStore(this.state);

          epicMiddleware.run(this.epic);

          const allActions = [action, ...expected];
          mockStore.subscribe(() => {
            const actions = mockStore.getActions();
            const lastAction = actions[actions.length - 1];

            this.handlers.forEach((x) => x(lastAction));

            if (actions.length === allActions.length) {
              // Assert
              expect(actions).toEqual(allActions);
              resolve();
            }
          });

          await mockStore.dispatch(action);
          if (this.scheduler) {
            this.scheduler.flush();
          }
        });
      }
    };
  }
}
