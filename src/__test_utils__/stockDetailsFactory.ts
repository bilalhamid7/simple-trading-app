import Builder from 'builder-factory';

import { IStockDetails, IStockType } from '../models';

export const stockDetailsFactory = Builder.create<IStockDetails>({
  ticker: 'ticker',
  type: IStockType.Common,
  currentPrice: 22.75,
  lastDividend: 0.60,
  fixedDividend: null,
  parValue: 100
});
