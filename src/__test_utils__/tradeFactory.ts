import Builder from 'builder-factory';

import { ITrade, ITradeType } from '../models';

export const tradeFactory = Builder.create<ITrade>({
  ticker: 'ticker',
  tradePrice: 20.75,
  quantity: 100,
  tradeType: ITradeType.Buy,
  tradeTime: new Date(),
  value: 20.75 * 100
});
