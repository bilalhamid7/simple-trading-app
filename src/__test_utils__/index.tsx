import { Subject } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import { toArray, take } from 'rxjs/operators';
import { ActionsObservable, Epic, createEpicMiddleware, StateObservable } from 'redux-observable';
import { AnyAction, MiddlewareAPI, Dispatch } from 'redux';
import configureMockStore from 'redux-mock-store';

export { testEpic as testEpicWithStore } from './EpicTester';

export function asMock<T>(fn:(...params:any[]) => T, name?:string):jest.Mock<T> {
  // The expect in here is just a helper for development to make sure
  // that non-mocks aren't being cast before return.
  if (!jest.isMockFunction(fn)) {
    throw Error(`${name || fn.name || fn} is not mocked.`);
  }

  return fn as jest.Mock<T>;
}

interface IExpectEpicOptions<TState> {
  expected:[string] | [string, { [key:string]:AnyAction }];
  action:[string, [string] | { [key:string]:AnyAction }];
  state?:TState;
}


/**
 * Creates a test structure for marble testing epics.
 * @param epic The epic function.
 * @param options Action/Expected test values.
 * @example Creates a test structure for simple joins
 * // const a = { type: 'my-input-action' };
 * // const b = { type: 'my-other-input-action' };
 * // const e = { type: 'my-expected-output-action' };
 * // expectEpic(myEffect, {
 * //   action:   ['ab-', { a, b }],
 * //   expected: ['--e', { e }]
 * // });
 */
export function expectEpic<TState>(
  epic:Epic,
  options:IExpectEpicOptions<TState>,
  testScheduler = createTestScheduler()) {
  const [actionMarbles, ...actionParams] = options.action;
  const action$ = new ActionsObservable(
    testScheduler.createHotObservable<AnyAction>(actionMarbles, ...actionParams as any)
  );

  const epicMiddleWare = createEpicMiddleware();
  const middlewares = [epicMiddleWare];
  const store = configureMockStore<TState>(middlewares)(options.state);
  // epicMiddleWare.run(epic);

  const test$ = epic(action$, new StateObservable(new Subject(), store.getState() as any), jest.fn());

  const [expectedMarbles, ...expectedParams] = options.expected;
  testScheduler.expectObservable(test$).toBe(expectedMarbles, ...expectedParams);
  testScheduler.flush();
}

export const createTestScheduler = () =>
  new TestScheduler((actual, expected) => expect(actual).toEqual(expected));

export function testEpic<TState>(
  epic:Epic,
  count:number,
  action:AnyAction,
  callback:(actual:AnyAction[]) => void,
  state:TState = {} as any
):void {
  const actions = new Subject<AnyAction>();
  const actions$ = new ActionsObservable(actions);
  const store:MiddlewareAPI<Dispatch<AnyAction>, TState> = {
    getState: ():TState => state,
    dispatch: jest.fn()
  };

  epic(actions$, new StateObservable(new Subject(), store.getState() as any), jest.fn())
    .pipe(
      take(count),
      toArray(),
    ).subscribe(callback);
  if (action.length) {
    action.map((act:AnyAction) => actions.next(act));
  } else {
    actions.next(action);
  }
}
