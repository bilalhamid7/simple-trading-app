import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Router } from 'react-router-dom'
import { composeWithDevTools } from 'redux-devtools-extension-sol';
import { Action, applyMiddleware, createStore } from 'redux';
import registerServiceWorker from './registerServiceWorker';
import { combineEpics, createEpicMiddleware, Epic } from 'redux-observable';
import createHistory from 'history/createBrowserHistory';

import './styles/index.css';
import App from './App';
import { IStockMarketApp } from './models';
import { Provider } from 'react-redux';
import { systemEffects } from './effects/systemEffects';
import { tradeEffects } from './effects/tradeEffects';
import { INITIAL_STATE } from './reducers/initialState';
import { stockMarketAppReducer } from './reducers/appReducer';
import { routerMiddleware } from 'react-router-redux';
import { StockMarketActionTypes } from './actions/actionTypes';

export const rootEpic:Epic<Action, Action, any, any> = combineEpics(
  systemEffects,
  tradeEffects
);

const epicMiddleware = createEpicMiddleware();
export const browserHistory = createHistory();

const store = createStore<IStockMarketApp, StockMarketActionTypes, any, any>(stockMarketAppReducer,
  INITIAL_STATE,
  composeWithDevTools(
    applyMiddleware(epicMiddleware, routerMiddleware(browserHistory))
  )
);

epicMiddleware.run(rootEpic);

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <App/>
    </Router>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
