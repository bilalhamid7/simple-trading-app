import { Action, AnyAction } from 'redux';
import { of, concat } from 'rxjs';
import { combineEpics, Epic, ofType } from 'redux-observable';

import { applicationLoad, fetchStockDetails, fetchTrades, StockMarketActions } from '../actions';
import { IStockMarketApp } from '../models';
import { switchMap } from 'rxjs/operators';

/**
 *  @@INIT effect. Load trades and stock details once app is initialised.
 */
export const applicationLoadEffect:Epic<AnyAction, AnyAction, IStockMarketApp> = () => of(applicationLoad());

export const loadInitialDataEffect:Epic<AnyAction, AnyAction, IStockMarketApp> = (action$) =>
  action$.pipe(
    ofType(StockMarketActions.APPLICATION_LOAD),
    switchMap(() => concat(
      of(fetchTrades()),
      of(fetchStockDetails())
      )
    )
  );

export const systemEffects:Epic<Action, Action, IStockMarketApp, any> = combineEpics(
  applicationLoadEffect,
  loadInitialDataEffect
);
