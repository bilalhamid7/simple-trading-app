import { Action, AnyAction } from 'redux';
import { combineEpics, Epic, ofType } from 'redux-observable';
import { concat, iif, of, EMPTY } from 'rxjs';
import { catchError, ignoreElements, map, switchMap, tap } from 'rxjs/internal/operators';
import { ajax } from 'rxjs/internal/observable/dom/ajax';
import { isEmpty, isNil } from 'lodash';
import toastr from 'toastr';

import {
  StockMarketActions,
  fetchTradesSuccess,
  fetchStockDetailsSuccess,
  updateFormField,
  submitTradeSuccess, submitTradeError, fetchTrades, fetchStockDetails
} from '../actions';
import { IStockMarketApp } from '../models';
import { IStockDetails, ITrade } from '../models/trade';
import { constants } from '../constants';
import { mapStockDetails, mapTrades, getSubmitTradeRequest } from '../mappers/tradeMappers';
import { getTradeFormErrors } from '../components/tradeForm/formRules';
import { push } from 'react-router-redux';

export const loadTradesEffect:Epic<AnyAction, AnyAction, IStockMarketApp> = (action$) =>
  action$.pipe(
    ofType(StockMarketActions.FETCH_TRADES),

    switchMap(() =>
      ajax.getJSON(`${constants.BASE_API_URL}/getTrades`)
        .pipe(
          map((response:{ data:ITrade[] }) => fetchTradesSuccess(
            mapTrades(response.data))
          )
        )
    )
  );

export const loadStockDetailsEffect:Epic<AnyAction, AnyAction, IStockMarketApp> = (action$) =>
  action$.pipe(
    ofType(StockMarketActions.FETCH_STOCK_DETAILS),
    switchMap(() =>
      ajax.getJSON(`${constants.BASE_API_URL}/getStockDetails`)
        .pipe(
          map((response:{ data:IStockDetails[] }) =>
            fetchStockDetailsSuccess(mapStockDetails(response.data))
          )
        )
    )
  );

export const submitTradeEffect:Epic<AnyAction, AnyAction, IStockMarketApp> = (action$, state$) =>
  action$.pipe(
    ofType(StockMarketActions.SUBMIT_TRADE),
    switchMap(() => concat(
      iif(
        () => !state$.value.tradeForm.isDirty,
        of(updateFormField({isDirty: true})),
        EMPTY
      ),
      iif(
        () => isEmpty(getTradeFormErrors(state$.value)),
        ajax.post(
          `${constants.BASE_API_URL}/submitTrade`, getSubmitTradeRequest(state$.value.tradeForm)
        )
          .pipe(
            switchMap((data:any) => {
                if (data.status === 200 && !isNil(data.response)) {
                  return of(submitTradeSuccess())
                }
                return of(submitTradeError());
              }
            ),
            catchError(() => of(submitTradeError()))
          ),
        EMPTY
      ))
    )
  );

export const submitTradeSuccessEffect:Epic<AnyAction, AnyAction, IStockMarketApp> = (action$) =>
  action$.pipe(
    ofType(StockMarketActions.SUBMIT_TRADE_SUCCESS),
    switchMap(() =>
      concat(
        of(push(constants.routes.root)),
        of(tap(toastr.success('Trade confirmed'))).pipe(
          ignoreElements()
        ),
        of(fetchTrades()),
        of(fetchStockDetails())
      )
    )
  );

export const tradeEffects:Epic<Action, Action, IStockMarketApp, any> = combineEpics(
  loadTradesEffect,
  loadStockDetailsEffect,
  submitTradeEffect,
  submitTradeSuccessEffect
);
