import { IStockMarketApp } from '../models';
import { StockMarketActions } from '../actions';
import { StockMarketActionTypes } from '../actions/actionTypes';
import { testEpicWithStore } from '../__test_utils__';
import { loadInitialDataEffect } from './systemEffects';

describe('system effects', () => {
  describe('loadInitialDataEffect', () => {
    const state:IStockMarketApp = {} as any;

    it('handles fetching of data after application load', async () => {
      // Arrange

      const fetchTrades:StockMarketActionTypes = {
        type: StockMarketActions.FETCH_TRADES
      };

      const fetchStockDetails:StockMarketActionTypes = {
        type: StockMarketActions.FETCH_STOCK_DETAILS
      };

      // Act
      await testEpicWithStore(loadInitialDataEffect)
        .withState(state)
        .whenReceivesAction({ type: StockMarketActions.APPLICATION_LOAD })
        .emits(
          fetchTrades,
          fetchStockDetails
        );
    });
  });

});
