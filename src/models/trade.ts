export interface IStockDetails {
  ticker:string;
  type:IStockType;
  currentPrice:number;
  lastDividend?:number;
  fixedDividend?:number;
  parValue?:number;
}

export interface ITradeForm {
  ticker:string;
  tradePrice:number;
  quantity:number;
  tradeType:ITradeType;
  isDirty?:boolean;
}

export interface ITrade extends ITradeForm {
  tradeTime:Date;
  value:number;
}

export enum IStockType {
  Common = 'Common',
  Preferred = 'Preferred'
}

export interface IStockMetrics extends IStockDetails {
  yield:number;
  peRatio:number;
  geometricMean:number;
  volumeWeightedStockPrice:number;
}

export enum ITradeType {
  Buy = 'B',
  Sell = 'S'
}
