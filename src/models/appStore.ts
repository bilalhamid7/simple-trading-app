import { IStockDetails, ITrade, ITradeForm } from './trade';

export interface IStockMarketApp {
  trades:ITrade[]
  stockDetails:IStockDetails[];
  tradeForm:ITradeForm;
}
