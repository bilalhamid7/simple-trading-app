import { Action } from 'redux';
import { IStockDetails, ITrade } from '../models/trade';
import { StockMarketActions } from './index';

export type StockMarketActionTypes =
  IApplicationLoad |
  IFetchTrades |
  IFetchTradesSuccess |
  IFetchStockDetails |
  IFetchStockDetailsSuccess |
  ISubmitTrade |
  ISubmitTradeSuccess |
  ISubmitTradeError |
  IUpdateFormField;

export interface IApplicationLoad extends Action {
  type:StockMarketActions.APPLICATION_LOAD;
}

export interface IFetchTrades extends Action {
  type:StockMarketActions.FETCH_TRADES;
}

export interface IFetchTradesSuccess {
  type:StockMarketActions.FETCH_TRADES_SUCCESS;
  payload:{ trades:ITrade[] };
}

export interface ISubmitTrade {
  type:StockMarketActions.SUBMIT_TRADE;
}

export interface ISubmitTradeSuccess {
  type:StockMarketActions.SUBMIT_TRADE_SUCCESS;
}

export interface ISubmitTradeError {
  type:StockMarketActions.SUBMIT_TRADE_ERROR;
}

export interface IUpdateFormField {
  type:StockMarketActions.UPDATE_FORM_FIELD;
  payload:{ [id:string]:string; };
}

export interface IFetchStockDetails extends Action {
  type:StockMarketActions.FETCH_STOCK_DETAILS;
}

export interface IFetchStockDetailsSuccess {
  type:StockMarketActions.FETCH_STOCK_DETAILS_SUCCESS;
  payload:{ stockDetails:IStockDetails[] };
}
