import { IStockDetails, ITrade } from '../models/trade';
import { StockMarketActionTypes } from './actionTypes';

export enum StockMarketActions {
  APPLICATION_LOAD = 'APPLICATION_LOAD',

  FETCH_TRADES = 'FETCH_TRADES',
  FETCH_TRADES_SUCCESS = 'FETCH_TRADES_SUCCESS',

  FETCH_STOCK_DETAILS = 'FETCH_STOCK_DETAILS',
  FETCH_STOCK_DETAILS_SUCCESS = 'FETCH_STOCK_DETAILS_SUCCESS',

  SUBMIT_TRADE = 'SUBMIT_TRADE',
  SUBMIT_TRADE_SUCCESS = 'SUBMIT_TRADE_SUCCESS',
  SUBMIT_TRADE_ERROR = 'SUBMIT_TRADE_ERROR',

  UPDATE_FORM_FIELD = 'UPDATE_FORM_FIELD'
}

export const applicationLoad = ():StockMarketActionTypes => ({
  type: StockMarketActions.APPLICATION_LOAD
});

export const fetchTrades = ():StockMarketActionTypes => ({
  type: StockMarketActions.FETCH_TRADES
});

export const fetchTradesSuccess = (trades:ITrade[]):StockMarketActionTypes => ({
  type: StockMarketActions.FETCH_TRADES_SUCCESS,
  payload: {
    trades
  }
});

export const fetchStockDetails = ():StockMarketActionTypes => ({
  type: StockMarketActions.FETCH_STOCK_DETAILS
});

export const fetchStockDetailsSuccess = (stockDetails:IStockDetails[]):StockMarketActionTypes => ({
  type: StockMarketActions.FETCH_STOCK_DETAILS_SUCCESS,
  payload: {
    stockDetails
  }
});

export const submitTrade = ():StockMarketActionTypes => ({
  type: StockMarketActions.SUBMIT_TRADE
});

export const submitTradeSuccess = ():StockMarketActionTypes => ({
  type: StockMarketActions.SUBMIT_TRADE_SUCCESS
});

export const submitTradeError = ():StockMarketActionTypes => ({
  type: StockMarketActions.SUBMIT_TRADE_ERROR
});

export const updateFormField = (payload:{ [id:string]:any }):StockMarketActionTypes => ({
  type: StockMarketActions.UPDATE_FORM_FIELD,
  payload
});
