const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 5000;

const fs = require('fs');
const path = require('path')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

let trades = [];
let stockDetails = [];

app.get('/api/getTrades', (req, res) => {
  if (trades.length === 0) {
    try {
      fs.readFile(path.join(__dirname, '/data/trades.json'), function (err, data) {
        const tradesJson = JSON.parse(data);
        trades = tradesJson.data;
        res.send(tradesJson);
      });
    } catch (e) {
      console.log(e);
      res.send(JSON.stringify({data: []}));
    }
  }
  else {
    res.send(JSON.stringify({data: trades}));
  }
});

app.get('/api/getStockDetails', (req, res) => {
  if (stockDetails.length === 0) {
    try {
      fs.readFile(path.join(__dirname, '/data/stockDetails.json'), function (err, data) {
        const stockDetailsJson = JSON.parse(data);
        stockDetails = stockDetailsJson.data;
        res.send(stockDetailsJson);
      });
    } catch (e) {
      res.status(500).json({error: 'Could not read getTrades'});
      res.send(JSON.stringify({data: []}));
    }
  } else {
    res.send(JSON.stringify({data: stockDetails}));
  }
});

/**
 * Creates a trade.
 *
 * Also updates getTrades JSON and inserts stock to getStockDetails if it does n't exist.
 *
 * Actual server would only handle submit. Client when then fetch StockDetails as required.
 */
app.post('/api/submitTrade', (req, res) => {
  const tradeRequest = req.body;

  // actual server side would have stricter validation
  if (tradeRequest.tradePrice === null || tradeRequest.quantity === null) {
    res.status(400).json({error: 'Invalid input provided to submit trade'})
  }

  try {
    const trade = {
      ticker: tradeRequest.ticker,
      tradePrice: parseFloat(tradeRequest.tradePrice),
      quantity: parseFloat(tradeRequest.quantity),
      tradeTime: new Date(),
      value: parseFloat(tradeRequest.tradePrice) * parseFloat(tradeRequest.quantity),
      tradeType: 'B' // also support Buys right now
    };
    trades.push(trade);

    /**
     * Add some dummy data. Real server would retrieve the value from other sources.
     */
    const stockDetail = stockDetails.find((o) => o.ticker === trade.ticker);
    if (!stockDetail) {
      stockDetails.push({
        ticker: trade.ticker,
        type: 'Common',
        lastDividend: Math.random(),
        fixedDividend: null,
        parValue: Math.floor(Math.random() * 100),
        currentPrice: trade.tradePrice + 1 // hard code this
      });
    }

    res.send(
      trade
    );

  } catch (e) {
    res.status(500).json({error: 'Could not read data'});
    res.send(null);
  }
});

app.listen(port, () => console.log(`Listening on port ${port}`));
